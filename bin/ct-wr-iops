#!/bin/bash
source /usr/share/ceph-tools/_ct-common

# Default Values
INTERVAL=10 # 10 secondes
NB=180     # environ 30 minutes (10*180 secondes)
TOT=$(( INTERVAL * NB / 60 ))
MINI=20000
WIDTH=75
help_msg () {
    printf """
usage: $0 [-h] [-i|--interval INTERVAL] [-n|--number NUMBER] [-w|--width WIDTH]

Description               Display semi-graphical progression of wr IOPS
  -h, --help              show this help message and exit
  -i INTERVAL, --interval INTERVAL
                          Interval in seconds between each measurement (default: ${INTERVAL} seconds)
  -n NUMBER, --number NUMBER
                          Number of measures before display (default : ${NB} measures -> ~${TOT} minutes)
  -m MINI, --mini MINI
                          Minimum IOPS in bytes/second (default : ${MINI} IOPS)
  -w WIDTH, --width
                          Widt of graph : (${WIDTH})

Example
  $0 -i 10 -n 180 -m 20000
"""
}


while [ $# -gt 0 ] ; do
    key="$1"
    case $key in
        -h|--help)
            help_msg
            ct_on_exit 0
            ;;
        -i|--interval)
            INTERVAL="$2"
            shift 2
            ;;
        -n|--number)
            NB="$2"
            shift 2
            ;;
        -m|--mini)
            MINI="$2"
            shift 2
            ;;
        -w|--width)
            WIDTH="$2"
            shift 2
            ;;
        *)
            echo "Arg not found : '$key'"
            ct_on_exit 1
    esac
done

WIDTH=$((WIDTH-1))
FACT=$(($MINI/10))

get_wr_iops() {
# Recupere les IOPS en ecriture
    IOPS=$(ceph -s -f json 2>/dev/null | jq ".pgmap.write_op_per_sec")
    if [ -z "$IOPS" ]
    then
        IOPS=0
    fi
    echo $IOPS
}

print() {
# Formatage de la sortie : 11000/13000/14000 op/s wr (Min/Avg/Max): -----------==|=
    GREEN="\e[32m"
    RED="\e[31m"
    YELLOW="\e[33m"
    BOLD="\e[1m"
    UNDER="\e[4m"
    NORM="\e[0m"
    IOMIN=$1
    IOAVG=$2
    IOMAX=$3
    COLOR=$GREEN
    if [ $IOMIN -lt $MINI ]
    then
      COLOR=$YELLOW
    fi
    if [ $IOAVG -lt $MINI ]
    then
      COLOR=$RED
    fi
    printf "\r$(date +'%x %X') Min %'d, ${UNDER}Avg %'d${NORM}, ${BOLD}Max %'d${NORM} op/s wr:   \t"  $IOMIN $IOAVG $IOMAX
    IOMIN=$((IOMIN/FACT))
    printf "${COLOR}"
    for i in $(seq 1 $IOMIN)
    do
       echo -n "-"
    done
    IOAVG=$((IOAVG/FACT))
    IOMAX=$((IOMAX/FACT))
    (( IOMIN += 1 ))
    for i in $(seq $IOMIN $IOAVG)
    do
       echo -n "="
    done
    (( IOMAX -= 1 ))
    IOM=0
    MAXI=0
    if  [ $IOMAX -gt $WIDTH ]
    then
        IOM=$WIDTH
        MAXI=1
    else
        IOM=$IOMAX
    fi
    echo -n "|"
    for i in $(seq $IOAVG $IOM)
    do
       echo -n "="
    done
    if [ $MAXI -eq 1 ]
    then
       echo -n "~"
    fi
    printf "${NORM}"
}

# Boucle infinie
while [ 1 ]
do
    # Initialisation Variables
    MIN=1000000000
    MAX=0
    TOTAL=0
    NBT=0
    for j in $(seq 1 $NB)
    do
        I=$(get_wr_iops)
        if [ $I -gt 0 ] && [ $I -lt 10000000 ]
        then
            ((TOTAL += I))
            ((NBT += 1))
            AVG=$((TOTAL/NBT))
            if [ $I -lt $MIN ]
            then
                MIN=$I
            fi
            if [ $I -gt $MAX ]
            then
                MAX=$I
            fi
            print $MIN $AVG $MAX
        fi
        sleep $INTERVAL
    done
    echo
done

ct_on_exit 0

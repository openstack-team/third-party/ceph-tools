#!/usr/bin/python3

import argparse
import json
from subprocess import run
from numpy import std

parser = argparse.ArgumentParser(prog="ct-osds-filling",
                                 description="Script for monitor osds_filling")
parser.add_argument("-m",
                    "--min",
                    action="store",
                    default=10,
                    type=int,
                    help="Display OSDs with utilisation < MIN%%")
parser.add_argument("-M",
                    "--max",
                    action="store",
                    default=80,
                    type=int,
                    help="Displax OSDs with utilization > MAX%%")
args = parser.parse_args()

c = run(["ceph", "osd", "df", "-f", "json"], capture_output=True)
osds = json.loads(c.stdout)
utilization = []
umin = {}
umax = {}
for osd in osds['nodes']:
    if osd['reweight'] > 0:
        utilization.append(round(osd['utilization']))
        if osd['utilization'] < args.min:
            umin[osd['id']] = osd['utilization']
        if osd['utilization'] > args.max:
            umax[osd['id']] = osd['utilization']

freq = {}
for items in utilization:
    freq[items] = (freq[items] + 1) if items in freq else 1
div = int(max(freq.values()) / 100) + 1

print("Utilization distribution:")
for k, v in sorted(freq.items()):
    nb = round(v / div)
    print(f"{k:2d}% -> {v:<5d}{nb * '#'}")

print(f"\nDeviation: {round(std(utilization),2)}")

print(f"\nOSDs with utilization < {args.min}%:")
for k, v in sorted(umin.items(), key=lambda x: x[1]):
    print(f"osd.{k:<4d}: {round(v,2)}%")
print(f"OSDs with utilization > {args.max}%:")
for k, v in sorted(umax.items(), key=lambda x: x[1]):
    print(f"osd.{k:<4d}: {round(v,2)}%")
